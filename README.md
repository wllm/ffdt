# Firefox DevTools Tips and Tricks

Chrome is very popular with web developers, and I'm guessing this is in large
part because of their excellent developer tools. However, Firefox is no slouch.

This document is a list of features I choose to highlight. Mozilla themselves
have a great resource on MDN for those who want to dig deeper:

https://developer.mozilla.org/en-US/docs/Tools

See these tips with some live examples by running the `demo/`.

## About Firefox versions

New features take some time before landing in the main Firefox release. You
may want to consider using one of the "early access" versions of Firefox, for
example the Developer Edition or even Nightly (equalivient to Chrome Canary).

All versions can be seen and downloaded here:

https://www.mozilla.org/en-US/firefox/channel/desktop/

See `desktop-entries/` in this repo for some tips on how to install Developer
Edition and Nightly on Linux.

In descending order of stability and ascending order of features:

* Beta
* Developer Edition
* Nightly

I've had success running Nightly as my everyday browser, so don't be afraid to
try! Developer Edition is a nice middle ground where you get features quicker
than mainstream while still being slightly more stable than Nightly.
