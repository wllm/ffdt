# Installing Developer Edition and Nightly on Linux

When you download either Developer Edition or Nightly for Linux on
https://www.mozilla.org/en-US/firefox/channel/desktop/ you get a .tar.bz2. In
order to get a menu entry for either edition in your desktop environment you
have to create a desktop entry. This directory includes an example for both
editions. All you need to do is update the paths.

## Extracting the archive

Just because I always forget the parameters to `tar`:

```
tar -xvf name-of-archive.tar.bz2
```

This should extract the contents to a folder named `firefox`.

## Choosing an install location

You are free to chose where to put the files, but I use the folder
`~/.local/bin/`.

```
mkdir -p ~/.local/bin/
mv firefox ~/.local/bin/
```

Note: if you want to install both Nightly and Developer Edition you need to
rename the `firefox` folder to something else, like `firefox-developer-edition`
and `nightly`.

## Creating a desktop entry

Copy the contents of either `nightly.desktop` or
`firefox-developer-edition.desktop` to `~/.local/share/applications/`. Edit
the `.desktop` file and update the paths in `Exec` and `Icon` so they point to
the firefox executable and the icon file respectively.

Close any running instance you might have of Firefox and search your menus for
the new entries. Run it and check Help -> About Firefox to verify the version
you are running.
