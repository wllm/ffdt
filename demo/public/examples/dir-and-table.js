(() => {
    const units = [
        { name: 'Dark Templar', race: 'Protoss' },
        { name: 'High Templar', race: 'Protoss' },
        { name: 'Immortal', race: 'Protoss' },
        { name: 'Archon', race: 'Protoss' },
        { name: 'Carrier', race: 'Protoss' },
        { name: 'Stalker', race: 'Protoss' },
        { name: 'Zealot', race: 'Protoss' },
        { name: 'Marine', race: 'Terran' },
        { name: 'Marauder', race: 'Terran' },
        { name: 'Hellion', race: 'Terran' },
        { name: 'Medevac', race: 'Terran' },
        { name: 'SCV', race: 'Terran' },
        { name: 'Siege Tank', race: 'Terran' },
        { name: 'Liberator', race: 'Terran' },
        { name: 'Viking', race: 'Terran' },
        { name: 'Thor', race: 'Terran' },
        { name: 'Queen', race: 'Zerg' },
        { name: 'Drone', race: 'Zerg' },
        { name: 'Zergling', race: 'Zerg' },
        { name: 'Roach', race: 'Zerg' },
        { name: 'Lurker', race: 'Zerg' },
        { name: 'Mutalisk', race: 'Zerg' },
        { name: 'Hydralisk', race: 'Zerg' },
        { name: 'Ultralisk', race: 'Zerg' },
    ];

    document
        .getElementById('example-5-dir')
        .addEventListener('click', () => {
            console.dir(units);
        });

    document
        .getElementById('example-5-table')
        .addEventListener('click', () => {
            console.table(units);
        });

    document
        .getElementById('example-5-log')
        .addEventListener('click', () => {
            console.log(units);
        });
})();
