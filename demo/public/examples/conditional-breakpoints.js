(() => {
    const units = [
        'Dark Templar',
        'High Templar',
        'Immortal',
        'Archon',
        'Carrier',
        'Stalker',
        'Zealot',
        'Marine',
        'Marauder',
        'Hellion',
        'Medevac',
        'SCV',
        'Siege Tank',
        'Liberator',
        'Viking',
        'Thor',
        'Queen',
        'Drone',
        'Zergling',
        'Roach',
        'Lurker',
        'Mutalisk',
        'Hydralisk',
        'Ultralisk',
    ];

    document
        .getElementById('example-1')
        .addEventListener('click', () => {
            units.forEach((unit) => {
                console.info(`${unit} OP`);
            });
        });
})();
