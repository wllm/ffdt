<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1.0">
    <title>Firefox DevTools Demo</title>
    <link rel="icon" href="/favicon.ico">
    <style>
        main {
            color: #444;
            font-size: 16px;
            margin: 40px auto;
            max-width: 650px;
            line-height: 1.6;
            padding: 0 10px;
        }

        h1,
        h2 {
            line-height: 1.2;
        }

        a {
            color: #0055e9;
        }

        .code {
            font-family: monospace;
        }

        .example {
            background-color: #efefef;
            padding: 45px 30px;
            position: relative;
        }

        .example-2 {
            position: absolute;
            top: 95px;
            right: 10px;
        }

        .button-group button {
            display: inline-block;
            margin-right: 8px;
        }
    </style>
</head>
<body>
<main>
    <h1>Firefox DevTools Tips and Tricks</h1>
    <p>
        Chrome is very popular with web developers, and I'm guessing this is
        in large part because of their excellent developer tools. However,
        Firefox is no slouch.
    </p>
    <p>
        This document is a list of features I choose to highlight. Mozilla
        themselves have a great resource on
        <a href="https://developer.mozilla.org/en-US/docs/Tools">MDN</a>
        for those who want to dig deeper.
    </p>

    <h2>Top tips</h2>
    <h3>Conditional breakpoints</h3>

    <a href="https://developer.mozilla.org/en-US/docs/Tools/Debugger/How_to/Set_a_conditional_breakpoint">
        MDN
    </a>

    <p>
        Say you're debugging a piece of code that takes a bunch of different
        parameters, but something weird is happening for one of them. Instead of
        setting a regular breakpoint and skip untill you get the input you want
        to debug you can set a <em>conditional</em> breakpoint that will only
        pause when a condition you set is true.
    </p>

    <p>
        You can set a regular breakpoint by clicking the line number in the
        debugger. To set a conditional breakpoint you _right click_ instead and
        pick Add Conditional Breakpoint. Now you enter a condition, like for
        instance <span class="code">unit === 'Zergling'</span>. Now the
        breakpoint will only pause when the value in unit equals
        <span class="code">'Zergling'</span>.
    </p>

    <div class="example">
        <p>
            Open the debugger and set a breakpoint in the snippet of code that
            loop over different Starcraft units. Set a conditional breakpoint
            where the value equals <span class="code">'Zergling'</span>.
        </p>
        <button id="example-1">Run example</button>
    </div>

    <h3>Drag 'n' dropp absolute positioned elements</h3>

    <a href="https://developer.mozilla.org/en-US/docs/Tools/Page_Inspector/How_to/Reposition_elements_in_the_page">
        MDN
    </a>

    <p>
        This one is pretty cool. Inspect an element with
        <span class="code">position: absolute;</span> and either
        <span class="code">top</span>, <span class="code">bottom</span>,
        <span class="code">left</span>, or <span class="code">right</span>,
        then look at the box model view. There should be a button in the
        low-right. Clicking it enters the browser in an edit state where the
        absolute positioned element can be dragged to where it should be. The
        containing element which the one you inspected is relative to is
        highlighted.
    </p>

    <div class="example">
        <p>
            The image in this example is absolute positioned relative to this
            example box. Try inspecting it and using the tool to drag it around.
        </p>
        <img class="example-2" src="images/zealot.png" />
    </div>

    <h3>Taking screenshots</h3>

    <a href="https://developer.mozilla.org/en-US/docs/Tools/Taking_screenshots">
        MDN
    </a>
    <p>
        When communicating about a feature or a bug with your teammates a
        screenshot goes a long way to help the one on the other end understand
        the issue. Firefox comes with powerful built-in screenshot tools.
    </p>
    <p>
        In the Settings tab of the developer tools there is an option to show the
        Take full-page screenshot button. Using this feature lets you take a
        screenshot of everything, even if something is outside of your viewport.
    </p>
    <p>
        In the same Settings tab you can also choose to have the screenshot
        copied to the clipboard. In JIRA you can <span class="code">Ctrl+P</span>
        when viewing an issue to paste the screenshot. No more looking around
        with the file picker.
    </p>
    <p>
        In the Inspect tab you can right-click an element and select Screenshot
        Node to only grab that node and its children. Great when discussing a
        particular feature or bug.
    </p>

    <div id="example-3" class="example">
        <p>
            Try taking a screenshot of just this example box by inspecting it
            and right clicking the node in the Inspect tab. Try taking a
            screenshot of the whole page as well. Did you add the button for
            it in the Settings? Try with the Developer Toolbar
            (<span class="code">Shift+F2</span>) as well! This box has an ID
            so screenshot it with the command <span class="code">screenshot --selector #example-3</span>.
        </p>
    </div>

    <h3>Edit and resend</h3>

    <a href="https://developer.mozilla.org/en-US/docs/Tools/Network_Monitor#Edit_and_Resend">
        MDN
    </a>
    <p>
        Say you're testing how a new API responds given slightly different inputs.
        Instead of opening a dedicated tool such as Postman or editing the client
        every time you can right click a request in the Network tab in Firefox
        and select Edit and resend.
    </p>
    <div class="example">
        <p>
            Open the Network tab and click the button below to start a fetch of
            the unit information page for the SCV. Try editing the request
            to fetch the info for the Marine. Note the different options you
            have for editing (headers, request body, method, and URL).
        </p>
        <button id="example-4">fetch</button>
    </div>

    <h3>
        <span class="code">console.table()</span> and
        <span class="code">console.dir()</span>
    </h3>

    <a href="https://developer.mozilla.org/en-US/docs/Tools/Web_Console/Console_messages#Logging">
        MDN
    </a>
    <p>
        Make sure to feature-check for these functions if using them in
        production code, and fall back to <span class="code">log</span>!
    </p>
    <p>
        Consider <span class="code">console.table()</span> when printing arrays
        or maps. This will display a nice looking table to the console which can
        be easier to read than a plain old
        <span class="code">console.log()</span>.
    </p>
    <p>
        When printing objects you can choose to use the non-standard
        <span class="code">console.dir()</span> supported by both Chrome and
        Firefox. <span class="code">dir()</span> prints out an interactive view
        of the given object. Chrome often does this with
        <span class="code">log()</span> as well, but
        <span class="code">dir()</span> behaves a bit differently. For example,
        printing a DOM node with <span class="code">log</span> prints an HTML
        tree structure, but printing the same node with
        <span class="code">dir</span> print an interactive object.
    </p>

    <div class="example">
        <p>
            Open the Console and click the buttons to see the difference in
            results with the same data.
        </p>
        <div class="button-group">
            <button id="example-5-dir">dir</button>
            <button id="example-5-table">table</button>
            <button id="example-5-log">log</button>
        </div>
    </div>

    <h2>Honorable mentions</h2>
    <ul>
        <li>
            Right click an element and spanss <span class="code">Q</span> to
            open the element in Page Inspector.
        </li>
        <li>
            Developer Toolbar (<span class="code">Shift+F2</span>)
            <a href="https://developer.mozilla.org/en-US/docs/Tools/GCLI">
                MDN
            </a>
        </li>
        <li>
            Scratchpad (<span class="code">Shift+F4</span>)
            <a href="https://developer.mozilla.org/en-US/docs/Tools/Scratchpad">
                MDN
            </a>
        </li>
        <li>
            The CSS grid inspector
            <a href="https://developer.mozilla.org/en-US/docs/Tools/Page_Inspector/How_to/Examine_grid_layouts">
                MDN
            </a>
        </li>
        <li>
            CSS shapes editor (coming in Firefox 60)
            <a href="https://developer.mozilla.org/en-US/docs/Tools/Page_Inspector/How_to/Edit_CSS_shapes">
                MDN
            </a>
        </li>
    </ul>
</main>
<script src="examples/conditional-breakpoints.js"></script>
<script src="examples/dir-and-table.js"></script>
<script src="examples/edit-and-resend.js"></script>
</body>
</html>
